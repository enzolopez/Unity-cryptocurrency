﻿using UnityEngine;
using UnityEngine.UI;

public class Example : MonoBehaviour
{
    public RawImage QrCode;
    public Text Uri;

	// Use this for initialization
	void Start ()
	{
	    QrCode.texture =
	        RPCManager.Instance.GetQrCode(RPCManager.Instance.MoneroWallets[0], RPCManager.Instance.MoneroRcpServers[0], 10000000, "d40d84a85101579c");
	    Uri.text = RPCManager.Instance.GetUri(RPCManager.Instance.MoneroWallets[0],
	       RPCManager.Instance.MoneroRcpServers[0], 10000000, "d40d84a85101579c");
        Debug.Log(RPCManager.Instance.GetTransferByTxId(RPCManager.Instance.MoneroRcpServers[0],"d40d84a85101579c"));
        Debug.Log(RPCManager.Instance.CheckPayment(RPCManager.Instance.MoneroRcpServers[0], "d40d84a85101579c"));
	}
}
