﻿using Newtonsoft.Json;

public class Currency
{
    private string _symbol;

	public string id {
		get;
		set;
	}

	public string name {
		get;
		set;
	}

	public string symbol
	{
	    get { return _symbol;}
	    set { _symbol = string.Format("{0}{1}", value.Substring(0, 1), value.Substring(1).ToLower()); }
	}

	public string rank {
		get;
		set;
	}

    [JsonProperty("price_usd")]
	public float price_usd {
		get;
		set;
	}

	public string price_btc {
		get;
		set;
	}

	[JsonProperty("24h_volume_usd")]
	public string volume_usd {
		get;
		set;
	}

	public string market_cap_usd {
		get;
		set;
	}

	public string available_supply {
		get;
		set;
	}

	public string total_supply {
		get;
		set;
	}

	public string max_supply {
		get;
		set;
	}

	public string percent_change_1h {
		get;
		set;
	}

	public string percent_change_24h {
		get;
		set;
	}

	public string percent_change_7d {
		get;
		set;
	}

	public string last_update {
		get;
		set;
	}
}

