﻿using UnityEngine;
using Newtonsoft.Json;

public static class CoinMarketCapApi
{
    public static void GetCurrencyInfo(string currencyToObtain, ref Currency currency)
    {
        using (WWW wc = new WWW(string.Format("https://api.coinmarketcap.com/v1/ticker/{0}/", currencyToObtain)))
        {
            while (!wc.isDone)
            {
            }
            currency = JsonConvert.DeserializeObject<Currency>(wc.text.Replace('[', ' ').Replace(']', ' '));
        }

    }
}

