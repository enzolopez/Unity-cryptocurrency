﻿using UnityEngine;

public class RPCManager : MonoBehaviour
{
    public static RPCManager Instance { get; private set; }

    public Wallet[] MoneroWallets;
    public MoneroServer[] MoneroRcpServers; 
    public bool UsingMonero;

    public decimal Price = 1;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }

    // Use this for initialization
    void Start()
    {
        if (MoneroWallets.Length == 0)
        {
            Debug.Log("There is no wallet, you need one for this active currency ");
        }
    }

    public Texture2D GetQrCode(Wallet wallet, MoneroServer server, long amount, string paymentId)
    {
        return server.GetQrCode(wallet.WalletAddress, amount, paymentId);
    }

    public string GetUri(Wallet wallet, MoneroServer server, long amount, string paymentId)
    {
        return server.GetUri(wallet.WalletAddress, amount, paymentId);
    }

    public string GetTransferByTxId(MoneroServer server, string txId)
    {
        return server.GetTransferByTxId(txId);
    }

    public bool CheckPayment(MoneroServer server,string paymentId)
    {
        var info = server.GetPayments(paymentId);
        return info != null && info["payment_id"].Equals(paymentId);
    }
}
