﻿using System;
using UnityEngine;

[Serializable]
[CreateAssetMenu(fileName = "RPC CONFIG", menuName = "CryptoCurrency/RPC Config File", order = 1)]
public class RPCConfig : ScriptableObject
{
    [Header("RCP Server Info")]
    [SerializeField] private string _domain;
    [SerializeField] private string _port;
    [SerializeField] private string _user;
    [SerializeField] private string _pass;

    public string Domain
    {
        get { return _domain; }
    }
		
    public string Port
    {
        get { return _port; }
    }
		
    public string User
    {
        get { return _user; }
    }
		
    public string Pass
    {
        get { return _pass; }
    }
}
