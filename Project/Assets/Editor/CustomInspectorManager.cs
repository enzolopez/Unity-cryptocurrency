﻿using System;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

[CustomEditor(typeof(RPCManager), true, isFallback = true)]
public class CustomInspectorManager : Editor
{
    RPCManager _target;

    Currency _monero;
    Texture _moneroIcon;
    private ReorderableList _moneroRcpServers;
    private ReorderableList _moneroWallets;

    private GUIStyle _header;

    private void OnEnable() 
    {
        _target = (RPCManager)target;
        InitializeMonero();
    }

    private void InitializeMonero()
    {
        SettingUpMoneroRpcServers();
        SettingUpMoneroWallets();
        CoinMarketCapApi.GetCurrencyInfo("monero", ref _monero);
        _moneroIcon = Resources.Load<Texture>("monero-logo");
    }

    private void SettingUpMoneroWallets()
    {
        var servers = serializedObject.FindProperty("MoneroWallets");
        _moneroWallets = new ReorderableList(serializedObject, servers, true,true,true,true);
        _moneroWallets.drawElementCallback += (rect, index, active, focused) =>
        {
            var prop = servers.GetArrayElementAtIndex(index);
            EditorGUI.PropertyField(rect, prop, new GUIContent());
        };
        _moneroWallets.drawHeaderCallback += (rect) => { EditorGUI.LabelField(rect, new GUIContent("Monero Wallets")); };

    }

    private void SettingUpMoneroRpcServers()
    {
        var servers = serializedObject.FindProperty("MoneroRcpServers");
        _moneroRcpServers = new ReorderableList(serializedObject, servers, true, true, true, true);
        _moneroRcpServers.drawElementCallback += (rect, index, isactive, isfocused) =>   
        {
            var prop = servers.GetArrayElementAtIndex(index);
            var fileprop = prop.FindPropertyRelative("config");
            EditorGUI.PropertyField(rect, fileprop, new GUIContent());
        };
        _moneroRcpServers.drawHeaderCallback += (rect) =>
        {
            EditorGUI.LabelField(rect,new GUIContent("Monero RPC Servers"));
        };
    }

    private void DrawMonero()
    {
        GUILayout.BeginVertical("box");
        GUILayout.BeginHorizontal();
        GUILayout.Label(_moneroIcon, new GUIStyle() { alignment = TextAnchor.MiddleCenter });
        GUILayout.Label("Monero",
            new GUIStyle()
            {
                fontSize = 24,
                alignment = TextAnchor.MiddleCenter,
                fontStyle = FontStyle.Bold,
                margin = new RectOffset() { top = 6 }
            });
        GUILayout.EndHorizontal();
        GUILayout.BeginVertical();
        _moneroRcpServers.DoLayoutList();
        _moneroWallets.DoLayoutList();
        DrawDataPrices(ref _monero);
        GUILayout.EndVertical();
        GUILayout.EndVertical();
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        DrawMonero();
        serializedObject.ApplyModifiedProperties();
    }

    private void DrawDataPrices(ref Currency currency)
    {

        GUILayout.BeginVertical("box");
        GUILayout.BeginHorizontal();
        GUILayout.Label("Calculator CoinMarketCap",
            new GUIStyle() { fontStyle = FontStyle.Bold, margin = new RectOffset() { top = 5 } });
        if (GUILayout.Button("Refresh"))
        {
            CoinMarketCapApi.GetCurrencyInfo("monero", ref _monero);
        }
        GUILayout.EndHorizontal();
        _target.Price = (decimal)EditorGUILayout.FloatField("Min Price", (float)_target.Price);
        GUILayout.Space(3);
        GUILayout.Label(string.Format("{0}/USD = {1}", currency.symbol.ToUpper(), currency.price_usd));
        GUILayout.Label(
            String.Format("Min. Value | USD = {0} | {1} = {2}",
                _target.Price, currency.symbol.ToUpper(), (_target.Price/(decimal) currency.price_usd).ToString().Substring(0,15)));


        GUILayout.EndVertical();
    }
}