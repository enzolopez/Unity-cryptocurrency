# Unity-Cryptocurrency

# Info

using [Json.net]

using [Unity] version: 2017.3

using [ZXing for Unity]

    
#### Working with
[![N|Solid](https://raw.githubusercontent.com/gregoiresgt/payment-icons/master/Assets/Payment/Monero/Monero_128.png)](http:/getmonero.org)


more incoming..

#### Installation

Download the Unity Package [Here]

Add the RPC Manager prefab to your scene and create the files for the server and the wallet

Assets -> Create -> CryptoCurrency

Add the files to your manager and play! <3
   
#### What is working
Multiples RPC servers can be used, same for wallets

* You can get the QR CODE and the Uri
* You can get the transfer data by txid
* You can get the payment and check it

i think with this you can check the transfer, so i'll work on some examples and a better interface for you


### Version

| Code Name | Launch | Unity |
| ------ | ------ | ------ | 
| ORISA | 13/01/2018 |  2017.3 |



   [Json.net]:<https://www.newtonsoft.com/json>
   [Unity]:<https://unity3d.com/>
   [Zxing for Unity]:<https://github.com/nenuadrian/qr-code-unity-3d-read-generate/>
   [Here]:<https://gitlab.com/AlekZombie/Unity-cryptocurrency/uploads/ac7266ada101c70434a533de7828ada4/unityCryptoCurrency.unitypackage>